What is this repository for?
A demo of angular2 dynamic form in AOT and Webpack

How do I get set up?

install: npm install

run: npm run start

JIT build: npm run build

AOT build: npm run build:aot

Contribution guidelines
Thanks angular webpack starter. This project begins with it. https://github.com/AngularClass/angular2-webpack-starter

Who do I talk to?
Repo owner or admin
Other community or team contact