import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { ChildComponent } from "./child.component";
@NgModule({
  imports:[
    ReactiveFormsModule,
    CommonModule
  ],
  declarations:[ChildComponent],
  exports:[ChildComponent]
})
export class DynamicFormModule{}
