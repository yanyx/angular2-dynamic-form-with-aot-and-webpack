/**
 * Created by Yunxuan Yan on 2017/2/3.
 */
import { Component, Input } from "@angular/core";
@Component({
  selector:"child-component",
  template:"<h2>{{childname}}</h2>"
})

export class ChildComponent{
  @Input() childname:string;
}
