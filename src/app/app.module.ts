import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
  NgModule,
} from '@angular/core';

/*
 * Platform and Environment providers/directives/pipes
 */
import { AppComponent } from './app.component';

import '../styles/styles.scss';
import { DynamicLoaderComponent } from "./dynamic-loader.component";

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    DynamicLoaderComponent
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    FormsModule,
    HttpModule
  ]
})
export class AppModule {}
