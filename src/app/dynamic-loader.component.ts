/**
 * Created by Yunxuan Yan on 2017/2/1.
 */

import {
  Component, ViewChild, ViewContainerRef, OnInit, NgModule, Compiler,
  ComponentFactoryResolver, Input, Injector, ReflectiveInjector
} from "@angular/core";
import { DynamicFormModule } from "./dynamic-form.module";
import { COMPILER_PROVIDERS } from "@angular/compiler";
@Component({
  selector:'dynamic-loader',
  template:'<div #dynamicAnchor></div>'
})

export class DynamicLoaderComponent implements OnInit{

  @ViewChild('dynamicAnchor',{read:ViewContainerRef}) dynamicAnchor:ViewContainerRef;

  protected injector:Injector;
  protected compiler:Compiler;

  constructor(
    protected compResolver:ComponentFactoryResolver,
    injector:Injector
    // protected compiler:Compiler
  ){
    this.injector = ReflectiveInjector.resolveAndCreate(COMPILER_PROVIDERS, injector);
    this.compiler = this.injector.get(Compiler);
  }

  ngOnInit(){

    //way1
    //AOT ERROR: Runtime compiler is not loaded
    let component = this.createNewComponent(require("./A.html"));
    let module = this.createComponentModule(component);
    this.compiler.compileModuleAndAllComponentsAsync(module)
      .then(({componentFactories})=>{
        const compFactory = componentFactories.find(x=>x.componentType === component);
        this.dynamicAnchor.clear();
        this.dynamicAnchor.createComponent(compFactory);
      })

    //way2
    //ERROR:No component factory found for e. Did you add it to @NgModule.entryComponents?
    // let component = this.createNewComponent(require("./A.html"));
    // let componentFactory = this.compResolver.resolveComponentFactory(component);
    // this.dynamicAnchor.clear();
    // this.dynamicAnchor.createComponent(componentFactory);
  }

  createNewComponent (tmpl:string) {
    @Component({
      selector: 'dynamic-component',
      templateUrl: tmpl
    })
    class CustomDynamicComponent {
      username = "Ryan";
    };
    // a component for this particular template
    return CustomDynamicComponent;
  }

  createComponentModule (componentType: any) {
    @NgModule({
      imports: [
        DynamicFormModule
      ],
      declarations: [
        componentType,
      ],
    })
    class RuntimeComponentModule
    {
    }
    // a module for just this Type
    return RuntimeComponentModule;
  }

}
